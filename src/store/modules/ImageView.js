export const ADD_IMAGEPOSTDATA = 'LOAD_IMAGEVIEWPOSTPROCESSED'

const initialState = {
  status: '',
  images: []
}

export default function reducer (state = initialState, action = {}) {
  switch (action.type) {
    case ADD_IMAGEPOSTDATA:
      return {
        ...state,
        images: [...state.images, action.payload]
      }
    default:
      return state
  }
}

export const addPost = data => dispatch => {
  dispatch({ type: ADD_IMAGEPOSTDATA, payload: data})

}