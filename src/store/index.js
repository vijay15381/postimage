import { combineReducers } from 'redux'
import ImageView from './modules/ImageView'

export default combineReducers({
  ImageView
})
