import React from 'react'
import { render } from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { applyMiddleware, createStore } from 'redux'
import reduxThunk from 'redux-thunk'
import JssProvider from 'react-jss/lib/JssProvider'
import App from './App'
import reducers from './store'

const store = createStore(
  reducers,
  {},
  applyMiddleware(reduxThunk),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

render(
  <Provider store={store}>
    <JssProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </JssProvider>
  </Provider>,
  document.getElementById('root')
)
