import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import Paper from '@material-ui/core/Paper';
import withStyles from '@material-ui/core/styles/withStyles';
import { addPost } from './store/modules/ImageView';

const styles = theme => ({
  main: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 400,
      marginLeft: 'auto',
      marginRight: 'auto',
    },
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
  },
  text: {
    textAlign: 'center',
    padding: '5px',
  }
});

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state={
      URL: '',
      Title: ''
    }
  }

render() {
  const { classes, images } = this.props;
  return (
    <main className={classes.main}>
      <CssBaseline />
      <Paper className={classes.paper}>
        <form className={classes.form}>
          <FormControl margin="normal" required fullWidth>
            <TextField
              id="outlined-search"
              label="Enter URL"
              type="search"
              className={classes.textField}
              margin="normal"
              variant="outlined"
              value={this.state.URL}
              onChange={event => this.setState({ URL: event.target.value})}
            />
            <TextField
              id="outlined-search"
              label="Title"
              type="search"
              className={classes.textField}
              margin="normal"
              variant="outlined"
              value={this.state.Title}
              onChange={event => this.setState({ Title: event.target.value})}
            />
          </FormControl>
          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() => {
              this.props.addPost({URL:this.state.URL, Title:this.state.Title})
              this.setState({ URL: '', Title: ''})
            }
          }
          >
            New Post
          </Button>
        </form>
          {images.map(option => (
            <div key={option.URL}>
              <div className={classes.text}>
                {option.Title}
              </div>
              <div>
                <img src={option.URL} height="300" width="400"/>
              </div>
            </div>
          ))}
      </Paper>
    </main>
  )};  
}

const mapStateToProps = state => ({
  images: state.ImageView.images
})

App.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(connect(
  mapStateToProps,
  {addPost}
)(App));
